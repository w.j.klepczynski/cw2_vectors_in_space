from scipy.stats import uniform
from scipy.stats import norm
import numpy as np
from csv import writer

def generate_points_xy(n1=2000):
    dist_x1 = norm(loc=200, scale=29)       #zmniejszając skalę uzyskuje się mniejszy szum
    dist_y1 = norm(loc=200, scale=29)
    dist_z1 = norm(loc=0.2, scale=0.05)

    x1=dist_x1.rvs(size=n1)
    y1=dist_y1.rvs(size=n1)
    z1=dist_z1.rvs(size=n1)

    points_xy = zip(x1, y1, z1)
    return points_xy


def generate_points_xz(n2=2000):
    dist_x2 = norm(loc=200, scale=29)
    dist_y2 = norm(loc=0.2, scale=0.05)
    dist_z2 = norm(loc=200, scale=29)

    x2=dist_x2.rvs(size=n2)
    y2=dist_y2.rvs(size=n2)
    z2=dist_z2.rvs(size=n2)

    points_xz = zip(x2, y2, z2)
    return points_xz


def generate_tube(r, n3=2000, h=150, cx=0, cy=0, cz=0):
    dx = uniform(cx - r, cx + 2 * r)
    dy = uniform(cy - r, cy + 2 * r)
    dz = uniform(cz, cz + h)

    n_corrected = int(np.ceil(n3 * 4 / np.pi))
    px = dx.rvs(size=n_corrected)
    py = dy.rvs(size=n_corrected)
    pz = dz.rvs(size=n_corrected)

    points = list(zip(px, py, pz))

    def filter_distances(point):
        section = (point[0] - cx, point[1] - cy)
        return np.linalg.norm(section) <= r

    points_in_tube = filter(filter_distances, points)
    return list(points_in_tube)


cloud_points = generate_tube(10)
cloud_points_xy = generate_points_xy(2000)
cloud_poinys_xz = generate_points_xz(2000)
with open('LidarData_separate.xyz', 'w', encoding='utf-8', newline='\n') as csvfile:
    csvwriter = writer(csvfile)
    # csvwriter.writerow('x', 'y', 'z')
    for p in cloud_points:
        csvwriter.writerow(p)

    for p1 in cloud_points_xy:
        csvwriter.writerow(p1)

    for p2 in cloud_poinys_xz:
        csvwriter.writerow(p2)