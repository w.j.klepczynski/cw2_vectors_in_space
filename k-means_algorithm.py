from scipy.stats import uniform
from scipy.stats import norm
import numpy as np
from csv import writer
import csv
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.metrics import confusion_matrix
from mpl_toolkits.mplot3d import axes3d
import pandas as pd



def generate_points_xy(n1=2000):                #utworzenie chmury punktów imitujących płaszczyznę XY
    dist_x1 = norm(loc=0, scale=100)
    dist_y1 = norm(loc=0, scale=100)
    dist_z1 = norm(loc=0.2, scale=0.05)

    x1=dist_x1.rvs(size=n1)
    y1=dist_y1.rvs(size=n1)
    z1=dist_z1.rvs(size=n1)

    points_xy = zip(x1, y1, z1)
    return points_xy


def generate_points_xz(n2=2000):                #utworzenie chmury punktów imitujących płaszczyznę XZ
    dist_x2 = norm(loc=0, scale=100)
    dist_y2 = norm(loc=0.2, scale=0.05)
    dist_z2 = norm(loc=0, scale=100)

    x2=dist_x2.rvs(size=n2)
    y2=dist_y2.rvs(size=n2)
    z2=dist_z2.rvs(size=n2)

    points_xz = zip(x2, y2, z2)
    return points_xz


def generate_tube(r, n3=2000, h=100, cx=0, cy=0, cz=0):         #utworzenie chmury punktów imitujących walec
    dx = uniform(cx - r, cx + 2 * r)
    dy = uniform(cy - r, cy + 2 * r)
    dz = uniform(cz, cz + h)

    n_corrected = int(np.ceil(n3 * 4 / np.pi))
    px = dx.rvs(size=n_corrected)
    py = dy.rvs(size=n_corrected)
    pz = dz.rvs(size=n_corrected)

    points = list(zip(px, py, pz))

    def filter_distances(point):
        section = (point[0] - cx, point[1] - cy)
        return np.linalg.norm(section) <= r

    points_in_tube = filter(filter_distances, points)
    return list(points_in_tube)


cloud_points = generate_tube(10)
cloud_points_xy = generate_points_xy(2000)
cloud_poinys_xz = generate_points_xz(2000)
with open('LidarData.xyz', 'w', encoding='utf-8', newline='\n') as csvfile:         #utworzenie pliku z wygenerowanymi punktami
    csvwriter = writer(csvfile)
    # csvwriter.writerow('x', 'y', 'z')
    for p in cloud_points:
        csvwriter.writerow(p)

    for p1 in cloud_points_xy:
        csvwriter.writerow(p1)

    for p2 in cloud_poinys_xz:
        csvwriter.writerow(p2)


space=pd.read_csv('LidarData.xyz')      #wczytanie zbioru space, powstały z pliku LidarData.xyz

Data = space.data
Classes = space.target

kmeans=KMeans(n_clusters=3)             #parametr algorytmu k-means - podział na 3 grupy

kmeans.fit(Data)                        #uczenie algorytmu k-means

print ('Uzyskane etykiety podziału dla zbioru space')     #uzyskany podział
podzial=kmeans.labels_
print (podzial)

fig=plt.figure(1)
wykr= axes3d (fig)
wykr.scatter(Data[:,0], Data[:,1], Data[:,2], c=podzial.astype(np.float))
plt.show()

print (confusion_matrix(podzial, Classes))



'''
def LidarData_reader():
    with open('LidarData.xyz', newline='') as csvfile:   #odczyt wygenerowanych punktów w pliku cvs
        reader = csv.reader(csvfile, delimiter=',')
        for x, y, z in reader:
            yield (float(x), float(y), float(z))

Lidar =[]
for p3 in LidarData_reader():
    Lidar.append(p3)

Lidar

'''


'''
def k_means(list_of_points):
    n_clusters = 3                                  #liczba oczekiwanych do otrzymania klastrów

    X = np.array(list_of_points)

    k_means = KMeans(n_clusters=n_clusters)
    k_means = k_means.fit(X)
    labels = k_means.predict(X)


    red = labels == 0                               #kolorowanie znalezionych klastrów punktów
    yellow = labels == 1
    blue = labels == 2
'''