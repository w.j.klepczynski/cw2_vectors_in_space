import csv
import matplotlib.pyplot as plt
import numpy as np
from sklearn.cluster import KMeans, DBSCAN
import pyransac3d as pyrsc
import random


def read_csv(f_name):       #odczyt pliku csv
    with open(file=f_name, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for x, y, z in reader:
            yield float(x), float(y), float(z)


def fit(pts_cld, hist=0.05, maxIter=1000):      #pobranie danych do klasyfikatora
    """
    Find the best equation for a plane.

    :param pts_cld - chmura punktów 3D jako `np.array (N,3)`.
    :param hist - Odległość progowa od płaszczyzny, która jest uważana za "w linii"" .
    :param maxIter - Maksymalna liczba iteracji, przez którą RANSAC wykona pętlę .
    :returns:
    - `self.equation`:  Parametry płaszczyzny opisanej za pomocą Ax+By+Cy+D `np.array (1, 4)`
    - `self.inliers`: Punkty ze zbioru uważane za "w linii".

    ---
    """
    n_points = pts_cld.shape[0]         #przypisanie wygenerowanych punktów z chmury
    print(n_points)
    best_equation = []
    best_inliers = []

    for it in range(maxIter):

        # Wybór 3 randomowych punktów za zbioru, na  podstawie których zostanie stworzona płasczyzna
        id_samples = random.sample(range(1, n_points - 1), 3)
        pt_samples = pts_cld[id_samples]

        # Wykorzystując wylosowane 3 punkty trzeba znaleźć równanie opisujace daną płasczyznę. Należy obliczyć odpowiednie wektory. Pierwsze 2 są częścią tej płasczyzny
        # vectorA = pt2 - pt1
        # vectorB = pt3 - pt1

        vectorA = pt_samples[1, :] - pt_samples[0, :]
        vectorB = pt_samples[2, :] - pt_samples[0, :]

        # Obliczenie wektora krzyżowego vectorA i vectorB, któryh wynikiem jest vectorC prostopadły do płaszczyzny
        vectorC = np.cross(vectorA, vectorB)

        # Równanie płaszczyzny opisane jest wzorem:  vecC[0]*x + vecC[1]*y + vecC[0]*z = -k. Żeby wyznaczyć k trzeba użyć punktu
        vectorC = vectorC / np.linalg.norm(vectorC)
        k = -np.sum(np.multiply(vectorC, pt_samples[1, :]))
        plane_eq = [vectorC[0], vectorC[1], vectorC[2], k]

        # Odległość od punktu do płaszczyzny
        # https://mathworld.wolfram.com/Point-PlaneDistance.html
        #pt_id_inliers = []       # lista identyfikatorów punktów w linii
        distance_pt = (plane_eq[0] * pts_cld[:, 0] + plane_eq[1] * pts_cld[:, 1] + plane_eq[2] * pts_cld[:, 2] + plane_eq[3]) / np.sqrt(plane_eq[0] ** 2 + plane_eq[1] ** 2 + plane_eq[2] ** 2)

        # Wybór identyfikatorów punktów, których odległość jest większa od założonej histerezy
        pt_id_inliers = np.where(np.abs(distance_pt) <= hist)[0]
        if (len(pt_id_inliers) > len(best_inliers)):
            best_equation = plane_eq
            best_inliers = pt_id_inliers
        inliers = best_inliers
        equation = best_equation

    return equation, inliers

def k_means(point_list):
    n_clusters = 3          #przewidywana ilość clastrów do wygenerowania

    X = np.array(point_list)        #utworzenie tablicy X wypełnionej punktami
    clusterer = KMeans(n_clusters=n_clusters)   #grupowanie K-średnich, wpisanie liczby clustrów do utworzenia i centroidów do wygenerowania
    y_pred = clusterer.fit_predict(X)       #grupowanie punktów na clustry

    red = y_pred == 0               #przypisanie kolorów do grup
    green = y_pred == 1
    blue = y_pred == 2

    fig2 = plt.figure()
    ax2 = fig2.add_subplot(projection='3d')         #rzutowanie punktów w pzrestrzeń 3D
    ax2.scatter(X[red, 0], X[red, 1], X[red, 2], marker='x')    #nadanie znaczników poszczególnym grupom
    ax2.scatter(X[green, 0], X[green, 1], X[green, 2], marker='*')
    ax2.scatter(X[blue, 0], X[blue, 1], X[blue, 2], marker='o')
    plt.show()


    #RANSAC
    print('Ransac:')
    best_equation, best_inliers=fit(X[red])
    print(best_equation)
    best_equation, best_inliers=fit(X[green])
    print(best_equation)
    best_equation, best_inliers=fit(X[blue])
    print(best_equation)

    print('pytansac:')          #z biblioteki pyransac3dalk
    plane1 = pyrsc.Plane()      #https://pypi.org/project/pyransac3d/
    best_equation, best_inliers = plane1.fit(X[red], 0.01)
    print(best_equation)
    plane2 = pyrsc.Plane()
    best_equation, best_inliers = plane2.fit(X[green], 0.01)
    print(best_equation)
    plane3 = pyrsc.Plane()
    best_equation, best_inliers = plane3.fit(X[blue], 0.01)
    print(best_equation)
    # cylinder1 = pyrsc.Cylinder()
    # center, axis, radius, inliers = cylinder1.fit(X[red], hist=0.2, maxIter=10000)
    # print(center, axis, radius)

def k_means_sci(point_list):
    #https://scikit-learn.org/stable/modules/generated/sklearn.cluster.DBSCAN.html
    #https://scikit-learn.org/stable/auto_examples/cluster/plot_dbscan.html
    X = np.array(point_list)            #stworzenie tablicy z chmury punktów
    clustering = DBSCAN(eps=15, min_samples=10).fit(X)  #clustrowanie tablicy X zawierającej punkty; eps - maksymalna odległość między próbkami aby były uznane za sąsiadujące; min_samples - liczba próbek w sąsiedztwie punktu, który należy uznać za centralny
    core_samples_mask = np.zeros_like(clustering.labels_, dtype=bool)
    core_samples_mask[clustering.core_sample_indices_] = True
    labels = clustering.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)

    print('Estimated number of clusters: %d' % n_clusters_)
    print('Estimated number of noise points: %d' % n_noise_)
    # Plot result
    # Black removed and is used for noise instead.
    unique_labels = set(labels)
    colors = [plt.cm.Spectral(each)
              for each in np.linspace(0, 1, len(unique_labels))]
    for k, col in zip(unique_labels, colors):
        if k == -1:
            # Black used for noise.
            col = [0, 0, 0, 1]

        class_member_mask = (labels == k)

        xy = X[class_member_mask & core_samples_mask]
        plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                 markeredgecolor='k', markersize=14)

        xy = X[class_member_mask & ~core_samples_mask]
        plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                 markeredgecolor='k', markersize=6)
    plt.title('Estimated number of clusters: %d' % n_clusters_)
    plt.show()


if __name__ == '__main__':         #import danych z pliku csv, chmury punktów
    points_csv = list(read_csv('LidarData_separate.xyz'))
    X, Y, Z = zip(*points_csv)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    ax.scatter(X, Y, Z)
    plt.show()
    k_means(points_csv)
    k_means_sci(points_csv)
